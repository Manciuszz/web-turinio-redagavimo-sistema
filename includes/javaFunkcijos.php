<script type="text/javascript">
	function _x(STR_XPATH) {
		var xresult = document.evaluate(STR_XPATH, document, null, XPathResult.ANY_TYPE, null);
		var xnodes = [];
		var xres;
		while (xres = xresult.iterateNext()) {
			xnodes.push(xres);
		}

		return xnodes;
	}

	function getPath(element) {
		if (element.id!=='')
			return '//*[@id="'+element.id+'"]';
		if (element===document.body)
			return "/html/"+element.tagName;

		var ix = 0;
		var siblings= element.parentNode.childNodes;
		for (var i= 0; i<siblings.length; i++) {
			var sibling= siblings[i];
			if (sibling===element)
				return getPath(element.parentNode)+'/'+element.tagName+'['+(ix+1)+']';
			if (sibling.nodeType===1 && sibling.tagName===element.tagName)
				ix++;
		}
	}
	
	function getContent() {
		var array = [];
		var writeLimit = 5;
		var narys = '<?php echo $_SESSION['narioGrupe']; ?>';
		$('[pakeista]').each(function(index) {
			var text = $(this).html();
			if (text.length > 0) {
				if (narys != "Vartotojas" || index <= writeLimit) {
					array[getPath($(this).first()[0]).toLowerCase()] = text;
				} else {
					alert("Pasiektas pakeitimų 'Vartotojo' limitas (" + writeLimit + ") -> " + (index+1));
				}
			}
		});
		return array;
	}
	
	function issaugotiHTML() {
		$.ajax({
			type: "POST",
			url: "/wtrs/main/issaugoti.php",
			data: { 
				"nuoroda": window.location.href,
				"narys": '<?php echo $_SESSION['narys']; ?>', // Klaida kai mano statusas = "Vartotojas"
				"data": (function() { 
					var content = getContent();
					var output = Object.keys(content).map(function(key) {
						return {selector: key, html: content[key]};
					});
					return JSON.stringify(output)
				})()
			},
			cache: false,
			success: function(response) {
				// console.log("IssaugotiHTML: \n" + response);
			}
		});
	}
	
	function uzkrautiHTML() {
		$.ajax({
			type: "POST",
			url: "/wtrs/main/uzkrauti.php",
			data: { 
				"url": '<?php echo $url; ?>'
			},
			cache: false,
			success: function(response) {
				// console.log("UzkrautiHTML: \n" + response);
				// if (response == "Perkrauti") {
					// window.location.reload();
				// }
				$('[pakeista]').removeAttr('pakeista');
				var array = $.parseJSON(response);
				$.each(array, function(key, value){
					$(_x(key)).html(value);
				});				
			}
		});
	}	
	
	function atnaujintiLoga() {	
		$.ajax({
			type: "POST",
			url: "/wtrs/main/sudarytiLoga.php",
			data: { 
				"url": '<?php echo $url; ?>'
			},
			cache: false,
			success: function(response) {
				// console.log("atnaujintiLoga: \n" + response);
				
				var array = $.parseJSON(response);
				var data = array.result1;
				var	data2 = array.result2;

				// console.log(data);
				// console.log(data2);
				
				var index = 0;
				$('body > div.popup > div > table').empty();
				$.each(data, function(username, array) {
					$.each(array, function(editor, grupe) {
						index++;
						if (!(index%1)) 
							tRow = $('<tr>');           
				   
						tCell = $('<td>').html(index + ". " + username + " (" + grupe + ")");									
						$('body > div.popup > div > table').append(tRow.append(tCell));
						
						var index2 = 0;
						$.each(data2, function(selector, array) {
							$.each(array, function(editor2, html_kodas) {
								if (editor == parseInt(editor2)) { // klaida
									index2++;
									if (!(index2 % 1)) 
										tRow = $('<tr>');		   
									
									tCell = $('<td>');
									tCell.attr("xpath", selector);
									tCell = tCell.html(html_kodas);
									
									$('body > div.popup > div > table > tbody > tr:nth-child(' + index + ') > td').append(tRow.append(tCell));
								}						
							});						
						});
						
					});								
				});					
			}
		});
	}
	
	function atsijungti() {
		$( "#barForm2 > input" ).click();
	}	

	$(document).ready(function() {
		uzkrautiHTML();
		
		var narys = '<?php echo $_SESSION['narioGrupe']; ?>';
		var divOpened = false;
		
		$('[data-popup-open]').on('click', function(e) {
			var targeted_popup_class = $(this).attr('data-popup-open');
			$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
			divOpened = true;
			
			e.preventDefault();
		});
	 
		$('[data-popup-close]').on('click', function(e) {
			var targeted_popup_class = $(this).attr('data-popup-close');
			$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
			divOpened = false;	

			if (narys == "Administratorius") { // Tik administratorius
				$.ajax({
					type: "POST",
					url: "/wtrs/main/issaugoti.php",
					data: { 
						"nuoroda": window.location.href,
						"narys": '<?php echo $_SESSION['narys']; ?>',
						"data": (function() { 
							var content = getContent();
							var output = Object.keys(content).map(function(key) {
								return {selector: key, html: content[key]};
							});
							$(output).each(function(key, value) { 
								var xpath = $(_x(output[key].selector)).attr('xpath');
								if (xpath != null)
									output[key].selector = $(_x(output[key].selector)).attr('xpath');
							});
							return JSON.stringify(output)
						})()
					},
					cache: false,
					success: function(response) {
						// console.log("Saving log changes: \n" + response);
						uzkrautiHTML();
					}
				});
			}
			e.preventDefault();
		});
		
		$(document).mouseout(function(event) {
			var unhoveredElement = event.target;
			if (divOpened && ($(unhoveredElement).attr('xpath') == null || narys != "Administratorius")) return false;
			$(unhoveredElement).css('border', '0px solid blue');
			if ($(unhoveredElement).attr("contenteditable")) {
				$(unhoveredElement).unbind('click'); // Resetinam "toggle"
				$(unhoveredElement).attr("contenteditable", null);
			 }
		});
		
		$(document).mouseover(function(event) {
		    var hoveredElement = event.target;	   		
			if (divOpened && ($(hoveredElement).attr('xpath') == null || narys != "Administratorius")) return false;
				   
			$(hoveredElement).css('border', '2px solid blue');

			if ( $(hoveredElement).is('a, a *') ) { // Nuorodos
				var link = $(hoveredElement).closest("a").attr("href"); // Gali buti apskliaustu su nuoroda nuotrauku, tai reikia paimti artimiausia link'a...
				if (link.indexOf('<?php echo $_SERVER['SERVER_NAME']; ?>') == -1)
					$(hoveredElement).closest("a").attr('href', ((link.indexOf("http") == -1) ? (window.location.href).substr(0, (window.location.href).length - 1) : "http://" + '<?php echo $_SERVER['SERVER_NAME']; ?>' + "/wtrs/?get=") + link);
			}

			$(hoveredElement).mousedown(function(event) {
				var clickedElement = event.target;
				if (divOpened && ($(clickedElement).attr('xpath') == null || narys != "Administratorius"))
					return false;
				$(clickedElement).css('border', '2px solid red');

				if( $(clickedElement).is("img") || $(clickedElement).css('background-image') != "none") { // Nuotraukos
					// $(clickedElement).attr("src","none");
					// $(clickedElement).css("background-image", "none");
					$(clickedElement).remove();
				} else if ( $(clickedElement).is('a, a *') ) { // Nuorodos
					var link = $(clickedElement).attr('href');
					$(clickedElement).removeAttr('href');

					$(clickedElement).toggle(function() { // Pirmas paspaudimas - redaguoti
						$(clickedElement).attr('href', link);
						$(clickedElement).attr("contenteditable", true);
						$(clickedElement).attr("pakeista", true);
					}, function() { // Antras paspaudimas - eiti i nuoroda
						$(clickedElement).attr('href', link);
						window.location.href = $(clickedElement).attr('href');							
					});
				} else if (!$(clickedElement).is('#barForm > input')) { // Tekstai
					$(clickedElement).attr("contenteditable", true);
					$(clickedElement).attr("pakeista", true);
				}		

				// console.log($(clickedElement).attr('href'));
			});
		});
	});
</script>