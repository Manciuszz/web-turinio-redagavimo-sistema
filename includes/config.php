<?php 
	session_start();
	
	// Duomenų bazės serverio duomenys
	define('DBHOST','my_database_host');
	define('DBUSER','my_database_user');
	define('DBPASS','my_database_password');
	define('DBNAME','wtrs');

	// Prisijungiam prie MySQL Duomenų bazės
	$conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
	if($conn->connect_error) {
		die('Prisijungimo klaida: (' . $conn->connect_errno . ') '. $conn->connect_error);
	}
	$_SESSION['conn'] = $conn;
?>