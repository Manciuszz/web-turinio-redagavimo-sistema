<?php

//log user in ---------------------------------------------------
function login($user, $pass){
	$conn = $_SESSION['conn'];
	
	//strip all tags from varible   
	$user = strip_tags($conn->real_escape_string($user));
	$pass = strip_tags($conn->real_escape_string($pass));

	$pass = md5($pass); // Security LEVEL: YOLO

	// check if the user id and password combination exist in database
	$sql = "SELECT * FROM `nariai` WHERE username = '$user' AND password = '$pass'";
	$result = $conn->query($sql) or die('Query failed. ' . $conn->error);
      
	if ($result->num_rows == 1) {
		// the username and password match,
		// set the session
		$_SESSION['authorized'] = true;
					  
		// Continue
		$narys = $result->fetch_object();
		$_SESSION['narys'] = $narys->id;
		$_SESSION['narioGrupe'] = $narys->grupe;
	} else {
		// define an error message
		$_SESSION['error'] = 'Netinkamas vartotojo vardas arba slaptažodis';
	}
}

function register($user, $pass, $ngrupe) {
	$conn = $_SESSION['conn'];

	//strip all tags from varible   
	$user = strip_tags($conn->real_escape_string($user));
	$pass = strip_tags($conn->real_escape_string($pass));
	$grupe = strip_tags($conn->real_escape_string($ngrupe));
	
	if (empty($user) || empty($pass)) {
		$_SESSION['error'] = "Užpildykite laukus!";
		return 0;
	} else if (strlen($pass) < 8) {
		$_SESSION['error'] = "Slaptažodis turi būti sudarytas iš daugiau nei 8 simbolių!";
		return 0;
	}
	
	$pass = md5($pass); // Security LEVEL: YOLO
		
	$sql = "SELECT * FROM `nariai` WHERE username = '$user'";
	$result = $conn->query($sql) or die('Query failed. ' . $conn->error);
	
	if ($result->num_rows == 0) { // Nera duomenų bazėje tokio vartotojo..
		$sql = "INSERT INTO `nariai` (`id`, `username`, `password`, `grupe`) VALUES (NULL, '$user', '$pass', '$grupe');";
		$result = $conn->query($sql) or die('Query failed. ' . $conn->error);
		$_SESSION['success'] = "Narys -> " . $user . " sėkmingai užregistruotas!";
	} else {
		$_SESSION['error'] = 'Vartotojas jau yra duomenų bazėje';
	}
	Header( "Refresh: 5; url=http://". $_SERVER['SERVER_NAME'] ."/wtrs/");
}

// Authentication
function logged_in() {
	if($_SESSION['authorized'] == true) {
		return true;
	} else {
		return false;
	}	
}

function login_required() {
	if(logged_in()) {	
		return true;
	} else {
		Header( "Location: http://". $_SERVER['SERVER_NAME'] ."/wtrs/" );
		exit();
	}	
}

function logout() {
	session_destroy();
	unset($_SESSION['authorized']);
	Header( "Location: http://". $_SERVER['SERVER_NAME'] ."/wtrs/" );
	exit();
}

// Render error messages
function messages() {
	$message = '';
	if (isset($_SESSION['success'])) {
		if($_SESSION['success'] != '') {
			$message = '<div class="msg-ok">'.$_SESSION['success'].'</div>';
			$_SESSION['success'] = '';
		}		
	}
		
	if (isset($_SESSION['error'])) {
		if($_SESSION['error'] != '') {
			$message = '<div class="msg-error">'.$_SESSION['error'].'</div>';
			$_SESSION['error'] = '';
		}
	}
    echo "$message";
}

function errors($error){
	if (!empty($error)) {
		$i = 0;
		while ($i < count($error)) {
			$showError.= "<div class=\"msg-error\">".$error[$i]."</div>";
			$i++;
		}
		echo $showError;
	}// close if empty errors
} // close function

?>