<?php	
	if (!isset($_SESSION['conn']))
		require('../includes/config.php');

	$editor = $conn->real_escape_string($_POST['narys']);
	$nuoroda = $conn->real_escape_string($_POST['nuoroda']);
	$data = json_decode($_POST['data'], true); 
	
	foreach($data as $e) {
		//Pasaliname 
		$selector = $conn->real_escape_string($e['selector']);
		$sql = "DELETE FROM `puslapiai` WHERE `selector`='$selector'";
		if ($conn->query($sql) === TRUE) {
			echo "Record deleted successfully" . "\n";
		} else {
			echo "Error deleting record: " . $conn->error . "\n";
		}
		
		//Atnaujiname
		$html_kodas = $conn->real_escape_string($e['html']);
		$sql = "INSERT INTO `puslapiai` (`id`, `nuoroda`, `selector`, `html_kodas`, `editor`) VALUES (NULL, '$nuoroda', '$selector', '$html_kodas', '$editor')";
		if ($conn->query($sql) === TRUE) {
			echo "Record inserted successfully" . "\n";
		} else {
			echo "Error inserting record: " . $conn->error . "\n";
		}
	}	
?>