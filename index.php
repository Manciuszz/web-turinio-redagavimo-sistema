<html>
<head> <title>WTRS</title> </head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<center>
	<link rel="stylesheet" href="styles/style.css" type="text/css" />
	<link rel="stylesheet" href="styles/logas.css" type="text/css" />

	<div id="browserBar">
		<div id="status">Prisijungta kaip: Vartotojas</div>
		<form id="barForm" method="get" action=""> 
			<input type="text" name="get" placeholder="http://www.pavyzdys.lt"> 
		</form>
		<form id="barForm2" method="post" action=""> <input type="submit" class="button" name="logout" value="Atsijungti" hidden> </form>
	</div>
	<?php 
		require("includes/config.php");
		require("includes/phpFunkcijos.php");
		
		if (isset($_POST['logout'])) {
			logout();
		}
	?>
	
	<div id="login-frame" class="lwidth">
		<div class="page-wrap">
			<div class="content">
				<?php 
					if(isset($_POST['submit'])) {
						login($_POST['username'], $_POST['password']);						
					} else if (!isset($_SESSION['authorized'])) { // Jei neprisijunges, laikykime kad mes priklausome "Vartotojas" grupei
						login("vartotojas", "vartotojas");
					} else if (isset($_POST['registration']) && isset($_SESSION['narioGrupe']) && $_SESSION['narioGrupe'] == "Administratorius") {
						register($_POST['username'], $_POST['password'], $_POST['selected_grupe']);						
					}
				?>

				<div id="login">
					<p><?php echo messages();?></p>
					<form method="post" action="">
						<?php 
							if (isset($_SESSION['narioGrupe']) && $_SESSION['narioGrupe'] == "Administratorius") {
								echo '<strong>Priskirti grupę: </strong>';
								echo '<select id="selected_grupe" name="selected_grupe" onchange="this.form.submit();">
									<option value="Administratorius">Administratorius</option>
									<option value="Moderatorius">Moderatorius</option>
									<option value="Vartotojas">Vartotojas</option>
								</select>';
							}
						?> 
						<p><label><strong>Vartotojas</strong><input type="text" name="username" /></label></p>
						<p><label><strong>Slaptažodis</strong><input type="password" name="password" /></label></p>
						<input type="submit" name="submit" class="button" value="Prisijungti" />                      
						<?php 
							if (isset($_SESSION['narioGrupe']) && $_SESSION['narioGrupe'] == "Administratorius")
								echo '<input type="submit" name="registration" class="button" value="Pridėti narį" />';
						?>   
					</form>	  	  
				</div>	
				<script>
					(function() {
						$('#status').text("Prisijungta kaip: " + 
							<?php 
								if (isset($_SESSION['narioGrupe']))
									echo json_encode($_SESSION['narioGrupe']); 
							?>
						);
					})();
				</script>
			</div>	
		</div>
		<div class="footer">&copy; <?php echo 'KTU » Mantas Murauskas IFF-4/2' . ' ' . date('Y');?> </div>	
	</div>
	
</center>

<script>
	function pradejauNarsyti() {
		$("#login-frame").remove();
		
		var bAtnaujinti = $('<input type="button" class="button" value="Atnaujinti" onClick="issaugotiHTML();"/>');
		var bUzkraukti = $('<input type="button" class="button" value="Užkrauti" onClick="uzkrautiHTML();"/>');
		$("#barForm").append(bAtnaujinti);
		$("#barForm").append(bUzkraukti);
		
		var narioGrupe = $('#status').text();
		if (narioGrupe != "Prisijungta kaip: Vartotojas") {
			var bLogas = $('<input type="button" class="button" value="Log\'as" onClick="atnaujintiLoga();" data-popup-open="popup-1"/>');
			var bAtsijungti = $('<input type="button" class="button" value="Atsijungti" onClick="atsijungti();"/>');
			$("#barForm").append(bLogas);
			$("#barForm").append(bAtsijungti);
		} else {
			var bNamai = $('<input type="button" class="button" value="Pagrindinis" onClick="location.href=' + "'<?php echo("http://" . $_SERVER['SERVER_NAME'] . "/wtrs/"); ?>'" + ';" />');
			$("#barForm").append(bNamai);
		}
	}
</script>

<?php
	/*
		Papildomos funkcijos
			a. Svetainės elemtnų redagavimas vyksta "Real-Time" būdu - DONE
			b. "Vartotojams" skirtas pakeitimu limitas - DONE
			c. Pakeitimu logas (Zinoti kas atliko pakeitimus) - DONE
	*/
	
	if (empty($_GET))
		die();
			
	$url = $_GET['get'];
	unset($_GET['get']);

	if (!empty($_GET)) {
		$kitiArgumentai = http_build_query($_GET);
		$url .= "&" . $kitiArgumentai;
	}

	// Sutvarkoma nuoroda
	if (preg_match("#https?://#", $url) === 0) {
		$url = 'http://'.(strpos($url, 'www.') !== false ? '' : 'www.').$url."/";
		Header( "Location: http://". $_SERVER['SERVER_NAME'] ."/wtrs/?get=".$url );
	}
	
	echo '<script type="text/javascript">',
		 'pradejauNarsyti();',
		 '</script>';

    require("main/uzkrauti.php");	
	require("includes/javaFunkcijos.php");
?>

<div class="popup" data-popup="popup-1">
	<div class="popup-inner">
		<center><h2>Pakeitimų log'as</h2></center>
		<table></table>
		<p><center><a data-popup-close="popup-1">Uždaryti</a></center></p>
		<a class="popup-close" data-popup-close="popup-1">x</a>
	</div>
</div>

</html>